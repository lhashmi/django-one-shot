from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_detailed = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detailed": todo_detailed,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoList()

    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def update_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm(instance=todolist)

    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def delete_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)

    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/createitem.html", context)


def edit_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", item.list.id)

    else:
        form = TodoItemForm(instance=item)

    context = {"form": form}
    return render(request, "todos/edititem.html", context)
